Create Database Final
go


Create Table Usuario(
id int primary key identity(1,1),
--puestoId int foreign key references Puesto(id),
nombre varchar(100) not null,
apellido varchar(100) not null,
cedula varchar(11) unique not null,
correo varchar(50) unique not null,
nombreUsuario varchar(100) unique not null,
contrasena varchar(500) not null,
estatus varchar(2),
borrado bit default 0,
fechaRegistro datetimeoffset default getDate(),
fechaModificacion datetimeoffset,
creadoPor int foreign key references Usuario(id),
modificadoPor int foreign key references Usuario(id)
)
go



Create Table Departamento(
id int primary key identity(1,1),
nombre varchar(100) not null,
estatus varchar(2),
borrado bit,
fechaRegistro datetimeoffset default getDate(),
fechaModificacion datetimeoffset,
creadoPor int foreign key references Usuario(id),
modificadoPor int foreign key references Usuario(id)
)
go


Create Table Puesto(
id int primary key identity(1,1),
departamentoId int foreign key references Departamento(id),
nombre varchar(100) not null,
estatus varchar(2),
Borrado bit,
fechaRegistro datetimeoffset default getDate(),
fechaModificacion datetimeoffset,
creadoPor int foreign key references Usuario(id),
modificadoPor int foreign key references Usuario(id)
)
go


Alter table Usuario
add puestoId int foreign key references Puesto(id)


Create Table Sla(
id int primary key identity(1,1),
descripcion varchar(200) not null,
cantidadHoras int not null,
estatus varchar(2),
borrado bit,
fechaRegistro datetimeoffset default getDate(),
fechaModificacion datetimeoffset,
creadoPor int foreign key references Usuario(id),
modificadoPor int foreign key references Usuario(id)
)
go


Create Table Prioridad(
id int primary key identity(1,1),
slaId int foreign key references Sla(id),
nombre varchar(100) not null,
estatus varchar(2),
borrado bit,
fechaRegistro datetimeoffset default getDate(),
fechaModificacion datetimeoffset,
creadoPor int foreign key references Usuario(id),
modificadoPor int foreign key references Usuario(id)
)
go



Create Table Incidente(
id int primary key identity(1,1),
usuarioReportaId int foreign key references Usuario(id),
usuarioAsignadoId int foreign key references Usuario(id),
prioridadId int foreign key references Prioridad(id),
departamentoId int foreign key references Departamento(id),
titulo varchar(200) not null,
descripcion varchar(200) not null,
fechaCierre datetimeoffset,
comentarioCierre varchar(500),
estatus varchar(2),
Borrado bit,
fechaRegistro datetimeoffset default getDate(),
fechaModificacion datetimeoffset,
creadoPor int foreign key references Usuario(id),
modificadoPor int foreign key references Usuario(id)
)
go


Create Table Historial(
id int primary key identity(1,1),
idIncidente int foreign key references Incidente(id),
comentario varchar(500) not null,
estatus varchar(2),
Borrado bit,
fechaRegistro datetimeoffset default getDate(),
fechaModificacion datetimeoffset,
creadoPor int foreign key references Usuario(id),
modificadoPor int foreign key references Usuario(id)
)
go

Create proc SpInsertUsuario
@puestoId int,
@nombre varchar(100) ,
@apellido varchar(100),
@cedula varchar(11) ,
@correo varchar(50) ,
@nombreUsuario varchar(100) ,
@contrasena varchar(500) ,
@estatus varchar(2)
as
		insert into Usuario( nombre,apellido,cedula,correo,nombreUsuario,contrasena,estatus,puestoId)
		values(@nombre,@apellido,@cedula,@correo,@nombreUsuario,@contrasena,@estatus,@puestoId)
go

Create proc SpUpdateUsuario
@id int,
@puestoId int,
@nombre varchar(100) ,
@apellido varchar(100),
@cedula varchar(11),
@contrasena varchar(500) ,
@estatus varchar(2),
@fechaModificacion datetimeoffset
as

update Usuario set 
nombre = @nombre,
puestoId = @puestoId,
apellido = @apellido,
cedula = @cedula,
contrasena = @contrasena,
estatus = @estatus,
fechaModificacion = @fechaModificacion
where id = @id
go

Create proc SpDeleteUsuario
@id int,
@borrado bit
as

update Usuario set 
borrado = @borrado
where id = @id
go

Create proc SpInsertDepartamento
@nombre varchar(100) ,
@estatus varchar(2)
as
		insert into Departamento( nombre,estatus)
		values(@nombre,@estatus)
go

Create proc SpUpdateDepartamento
@id int,
@nombre varchar(100) ,
@estatus varchar(2),
@fechaModificacion datetimeoffset
as 

update Departamento set 
nombre = @nombre,
estatus = @estatus,
fechaModificacion = @fechaModificacion
where id = @id
go

Create proc SpDeleteDepartamento
@id int,
@borrado bit
as

update Departamento set 
borrado = @borrado
where id = @id
go


Create proc SpInsertPuesto
@departamentoId int,
@nombre varchar(100) ,
@estatus varchar(2)
as
		insert into Puesto(departamentoId,nombre,estatus)
		values(@departamentoId,@nombre,@estatus)
go

Create proc SpUpdatePuesto
@id int,
@departamentoId int,
@nombre varchar(100) ,
@estatus varchar(2),
@fechaModificacion datetimeoffset
as 

update Puesto set 
departamentoId = @departamentoId,
nombre = @nombre,
estatus = @estatus,
fechaModificacion = @fechaModificacion
where id = @id
go

Create proc SpDeletePuesto
@id int,
@borrado bit
as

update Puesto set 
borrado = @borrado
where id = @id
go

Create proc SpInsertSla
@descripcion varchar(200),
@cantidadHoras int ,
@estatus varchar(2),
@creadoPor int 
as
		insert into Sla(descripcion,cantidadHoras,estatus,creadoPor)
		values(@descripcion,@cantidadHoras,@estatus,@creadoPor)
go

Create proc SpUpdateSla
@id int,
@descripcion int,
@cantidadHoras int ,
@estatus varchar(2),
@fechaModificacion datetimeoffset,
@modificadoPor int 
as 

update Sla set 
descripcion= @descripcion,
cantidadHoras = @cantidadHoras,
estatus = @estatus,
fechaModificacion = @fechaModificacion,
modificadoPor  = @modificadoPor
where id = @id
go

Create proc SpDeleteSla
@id int,
@borrado bit
as

update Sla set 
borrado = @borrado
where id = @id
go

Create proc SpInsertPrioridad
@slaId int,
@nombre varchar(100) ,
@estatus varchar(2),
@creadoPor int 
as
		insert into Prioridad(slaId,nombre,estatus,creadoPor)
		values(@slaId,@nombre,@estatus,@creadoPor)
go

Create proc SpUpdatePrioridad
@id int,
@slaId int,
@nombre varchar(100) ,
@estatus varchar(2),
@fechaModificacion datetimeoffset,
@modificadoPor int 
as 

update Prioridad set 
slaId = @slaId,
nombre = @nombre,
estatus = @estatus,
fechaModificacion = @fechaModificacion,
modificadoPor  = @modificadoPor
where id = @id
go

Create proc SpDeletePrioridad
@id int,
@borrado bit
as

update Prioridad set 
borrado = @borrado
where id = @id
go

Create proc SpInsertIncidente
@usuarioReportaId int,
@usuarioAsignadoId int,
@prioridadId int,
@departamentoId int,
@titulo varchar(200),
@descripcion varchar(200),
@fechaCierre datetimeoffset,
@comentarioCierre varchar(500),
@estatus varchar(2),
@creadoPor int 
as
		insert into Incidente(usuarioReportaId,usuarioAsignadoId,prioridadId,departamentoId, titulo,descripcion,fechaCierre,comentarioCierre,estatus,creadoPor)
		values(@usuarioReportaId,@usuarioAsignadoId,@prioridadId,@departamentoId,@titulo,@descripcion,@fechaCierre,@comentarioCierre,@estatus,@creadoPor)
go

Create proc SpUpdateIncidente
@id int,
@usuarioReportaId int,
@usuarioAsignadoId int,
@prioridadId int,
@departamentoId int,
@titulo varchar(200),
@descripcion varchar(200),
@fechaCierre datetimeoffset,
@comentarioCierre varchar(500),
@estatus varchar(2),
@fechaModificacion datetimeoffset,
@modificadoPor int 
as 

update Incidente set 
usuarioReportaId = @usuarioReportaId ,
usuarioAsignadoId=@usuarioAsignadoId ,
prioridadId = @prioridadId,
departamentoId = @departamentoId,
titulo = @titulo ,
descripcion = @descripcion ,
fechaCierre = @fechaCierre ,
comentarioCierre = @comentarioCierre,
estatus = @estatus,
fechaModificacion = @fechaModificacion,
modificadoPor  = @modificadoPor
where id = @id
go

Create proc SpDeleteIncidente
@id int,
@borrado bit
as

update Incidente set 
borrado = @borrado
where id = @id
go

Create proc SpHistorial
@idIncidente int,
@comentario varchar(500),
@estatus varchar(2),
@creadoPor int 
as
		insert into Historial(idIncidente, comentario,estatus, creadoPor)
		values(@idIncidente, @comentario,@estatus, @creadoPor)
go