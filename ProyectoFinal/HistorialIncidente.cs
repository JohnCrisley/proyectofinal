﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace ProyectoFinal
{
    class HistorialIncidente
    {
        int _Id;
        int _IncidenteId;
        string _Comentario;
        string _Estatus;
        string _Borrado;
        string _FechaRegistrado;
        string _FechaModificacion;
        string _CreadorPor;
        string _ModificadoPor;
        public HistorialIncidente()
        {

        }

        public HistorialIncidente(int id, int incidenteId, string comentario, string estatus, string borrado, string fechaRegistrado, string fechaModificacion, string creadorPor, string modificadoPor)
        {
            Id = id;
            IncidenteId = incidenteId;
            Comentario = comentario;
            Estatus = estatus;
            Borrado = borrado;
            FechaRegistrado = fechaRegistrado;
            FechaModificacion = fechaModificacion;
            CreadorPor = creadorPor;
            ModificadoPor = modificadoPor;
        }

        public int Id { get => _Id; set => _Id = value; }
        public int IncidenteId { get => _IncidenteId; set => _IncidenteId = value; }
        public string Comentario { get => _Comentario; set => _Comentario = value; }
        public string Estatus { get => _Estatus; set => _Estatus = value; }
        public string Borrado { get => _Borrado; set => _Borrado = value; }
        public string FechaRegistrado { get => _FechaRegistrado; set => _FechaRegistrado = value; }
        public string FechaModificacion { get => _FechaModificacion; set => _FechaModificacion = value; }
        public string CreadorPor { get => _CreadorPor; set => _CreadorPor = value; }
        public string ModificadoPor { get => _ModificadoPor; set => _ModificadoPor = value; }

        public void add(Login login)
        {
            var item = new Conexion();
            string nombre = "", estatus = "";
            int IncidenteId = 0, lon = 0;

            do
            {
                Console.Write("\n\t\tElige del un incidente: ");
                try
                {
                    string md = "Select * from Incidente where borrado like '0'";
                    string mdd = "Select * from Incidente ";
                    DataSet d = item.Ejecutar(md);
                    DataSet dc = item.Ejecutar(mdd);
                    DataTable table = null;
                    DataTable table2 = null;

                    if (d.Tables.Count > 0)
                    {
                        table = d.Tables[0];
                        table2 = dc.Tables[0];
                        lon = table2.Rows.Count;
                    }

                    Console.WriteLine("Lista de Incidentes");
                    foreach (DataRow drCurrent in table.Rows)
                    {
                        Console.WriteLine("{0}- Titulo: {1}, Description: {2}, Comentario: {3}", Convert.ToInt32(drCurrent["id"].ToString()),
                            drCurrent["titulo"].ToString(), drCurrent["descripcion"].ToString(), drCurrent["comentarioCierre"].ToString());
                    }
                    IncidenteId = Convert.ToInt16(Console.ReadLine());
                }
                catch (Exception e)
                {
                    Console.WriteLine("\n\t\t\tEntra un numero...");
                }
            } while (IncidenteId < 0 || IncidenteId > lon);

            Console.Write("\n\t\tDigite el comentario: ");
            nombre = Console.ReadLine();
            Console.Write("\n\t\tDigite el estatus:  ");
            estatus = Console.ReadLine();

            string cmd = string.Format("Exec SpHistorial '{0}','{1}','{2}','{3}'",
              IncidenteId, nombre, estatus, login.Id);
            DataSet ds = item.Ejecutar(cmd);

            Console.Clear();
            Console.WriteLine("Se agregó un Historial de Incidente");
            Console.WriteLine("Presione una tecla para continuar..");
            Console.ReadKey();
            Console.Clear();
        }

        public void update(Login login)
        {
            var item = new Conexion();
            string nombre = "", estatus = "";
            int IncidenteId = 0, lon = 0;

            do
            {
                Console.Write("\n\t\tElige un Historial: ");
                try
                {
                    string md = "Select * from Historial where borrado like '0'";
                    string mdd = "Select * from Historial ";
                    DataSet d = item.Ejecutar(md);
                    DataSet dc = item.Ejecutar(mdd);
                    DataTable table = null;
                    DataTable table2 = null;
             
                    if (d.Tables.Count > 0)
                    {
                        table = d.Tables[0];
                        table2 = dc.Tables[0];
                        lon = table2.Rows.Count;
                    }
                    
                    Console.WriteLine("Historial");
                    foreach (DataRow drCurrent in table.Rows)
                    {
                        Console.WriteLine("{0}- Comentario: {1}, Estatus: {2}", Convert.ToInt32(drCurrent["id"].ToString()),
                            drCurrent["comentario"].ToString(), drCurrent["estatus"].ToString());
                    }
                    IncidenteId = Convert.ToInt16(Console.ReadLine());
                }
                catch (Exception e)
                {
                    Console.WriteLine("\n\t\t\tEntra un numero...");
                }
            } while (IncidenteId < 0 || IncidenteId > lon);

            Console.Write("\n\t\tDigite el comentario: ");
            nombre = Console.ReadLine();
            Console.Write("\n\t\tDigite el estatus:  ");
            estatus = Console.ReadLine();

            using (SqlConnection conn = new SqlConnection(Conexion.con))
            {
                using (SqlCommand cmd = new SqlCommand("SpUpdateHistorial", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("@id", IncidenteId));
                    cmd.Parameters.Add(new SqlParameter("@comentario", nombre));
                    cmd.Parameters.Add(new SqlParameter("@estatus", estatus));
                    cmd.Parameters.Add(new SqlParameter("@fecha", DateTime.Now));
                    cmd.Parameters.Add(new SqlParameter("@creadoPor", login.Id));


                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
            /*string cmd = string.Format("Exec SpUpdateHistorial '{0}','{1}','{2}','{3}','{4}'",
              IncidenteId, nombre, estatus, DateTime.Now, login.Id);
            DataSet ds = item.Ejecutar(cmd);*/

            Console.Clear();
            Console.WriteLine("Se actualizó el Historial de Incidente");
            Console.WriteLine("Presione una tecla para continuar..");
            Console.ReadKey();
            Console.Clear();
        }

        public void Retrieve()
        {
            var item = new Conexion();
            int IncidenteId = 0, lon = 0;

            do
            {
                Console.Write("\n\t\tElige uno de los incidente: ");
                try
                {
                    string md = "Select * from Incidente where borrado like '0'";
                    string mdd = "Select * from Incidente ";
                    DataSet d = item.Ejecutar(md);
                    DataSet dc = item.Ejecutar(mdd);
                    DataTable table = null;
                    DataTable table2 = null;

                    if (d.Tables.Count > 0)
                    {
                        table = d.Tables[0];
                        table2 = dc.Tables[0];
                        lon = table2.Rows.Count;
                    }

                    Console.WriteLine("Lista de Incidentes");
                    foreach (DataRow drCurrent in table.Rows)
                    {
                        Console.WriteLine("{0}- Titulo: {1}, Description: {2}, Comentario: {3}", Convert.ToInt32(drCurrent["id"].ToString()),
                            drCurrent["titulo"].ToString(), drCurrent["descripcion"].ToString(), drCurrent["comentarioCierre"].ToString());
                    }
                    IncidenteId = Convert.ToInt16(Console.ReadLine());
                }
                catch (Exception e)
                {
                    Console.WriteLine("\n\t\t\tEntra un numero...");
                }
            } while (IncidenteId < 0 || IncidenteId > lon);

            string cmd = $"SELECT* FROM Historial WHERE idIncidente like {IncidenteId} AND Borrado = 0";
            DataSet ds = item.Ejecutar(cmd);
            DataTable tabl = null;

            if (ds.Tables.Count > 0)
            {
                tabl = ds.Tables[0];
            }

            Console.WriteLine("Historial");
            foreach (DataRow drCurrent in tabl.Rows)
            {
                Console.WriteLine("{0}- Comentario: {1}, Estatus: {2}", Convert.ToInt32(drCurrent["id"].ToString()),
                    drCurrent["comentario"].ToString(), drCurrent["estatus"].ToString());
            }

            Console.WriteLine("");
            Console.WriteLine("Presione una tecla para continuar..");
            Console.ReadKey();
            Console.Clear();
        }

        public void Delete()
        {
            var item = new Conexion();
            int IncidenteId = 0, lon = 0, id = 0;

            do
            {
                Console.Write("\n\t\tElige uno de los incidente: ");
                try
                {
                    string md = "Select * from Incidente where borrado like '0'";
                    string mdd = "Select * from Incidente ";
                    DataSet db = item.Ejecutar(md);
                    DataSet dc = item.Ejecutar(mdd);
                    DataTable table = null;
                    DataTable table2 = null;

                    if (db.Tables.Count > 0)
                    {
                        table = db.Tables[0];
                        table2 = dc.Tables[0];
                        lon = table2.Rows.Count;
                    }

                    Console.WriteLine("Lista de Incidentes");
                    foreach (DataRow drCurrent in table.Rows)
                    {
                        Console.WriteLine("{0}- Titulo: {1}, Description: {2}, Comentario: {3}", Convert.ToInt32(drCurrent["id"].ToString()),
                            drCurrent["titulo"].ToString(), drCurrent["descripcion"].ToString(), drCurrent["comentarioCierre"].ToString());
                    }
                    IncidenteId = Convert.ToInt16(Console.ReadLine());
                }

                catch (Exception e)
                {
                    Console.WriteLine("\n\t\t\tEntra un numero...");
                }
            } while (IncidenteId < 0 || IncidenteId > lon);

            string cmd = $"SELECT* FROM Historial WHERE idIncidente like {IncidenteId} AND Borrado = 0";
            DataSet ds = item.Ejecutar(cmd);
            DataTable tabl = null;

            if (ds.Tables.Count > 0)
            {
                tabl = ds.Tables[0];
            }

            Console.WriteLine("Historial");
            foreach (DataRow drCurrent in tabl.Rows)
            {
                Console.WriteLine("{0}- Comentario: {1}, Estatus: {2}", Convert.ToInt32(drCurrent["id"].ToString()),
                    drCurrent["comentario"].ToString(), drCurrent["estatus"].ToString());
            }
            do
            {
                Console.Write("\n\t\tElige un Historial: ");
                try
                {
                    string md = "Select * from Historial where borrado like '0'";
                    string mdd = "Select * from Historial ";
                    DataSet db = item.Ejecutar(md);
                    DataSet dc = item.Ejecutar(mdd);
                    DataTable table = null;
                    DataTable table2 = null;

                    if (db.Tables.Count > 0)
                    {
                        table = db.Tables[0];
                        table2 = dc.Tables[0];
                        lon = table2.Rows.Count;
                    }
                    Console.WriteLine("Historial");

                    IncidenteId = Convert.ToInt16(Console.ReadLine());
                    lon = table.Rows.Count;
                }
                catch (Exception e)
                {
                    Console.WriteLine("\n\t\t\tEntra un numero...");
                }
            } while (id < 0 || id > lon);

            string d = string.Format("Exec SpDeleteHistorial '{0}','{1}'",
                IncidenteId, 1);
            DataSet g = item.Ejecutar(d);

            Console.Clear();
            Console.WriteLine("Se eliminó el Historial de Incidente");
            Console.WriteLine("Presione una tecla para continuar..");
            Console.ReadKey();
            Console.Clear();
        }
    }
}
