﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace ProyectoFinal
{
    class Incidente
    {
        int _Id;
        int _UsuarioReportaId;
        int _UsuarioAsignadoId;
        int _Prioridad;
        int _DepartamentoId;
        int _Titulo;
        string _Descripcion;
        string _FechaCierre;
        string _ComentarioCierre;
        string _Estatus;
        string _Borrado;
        string _FechaRegistrado;
        string _FechaModificacion;
        string _CreadorPor;
        string _ModificadoPor;
        public Incidente()
        {

        }

        public Incidente(int id, int usuarioReportaId, int usuarioAsignadoId, int prioridad, int departamentoId, int titulo, string descripcion, string fechaCierre, string comentarioCierre, string estatus, string borrado, string fechaRegistrado, string fechaModificacion, string creadorPor, string modificadoPor)
        {
            _Id = id;
            _UsuarioReportaId = usuarioReportaId;
            _UsuarioAsignadoId = usuarioAsignadoId;
            _Prioridad = prioridad;
            _DepartamentoId = departamentoId;
            _Titulo = titulo;
            _Descripcion = descripcion;
            _FechaCierre = fechaCierre;
            _ComentarioCierre = comentarioCierre;
            _Estatus = estatus;
            _Borrado = borrado;
            _FechaRegistrado = fechaRegistrado;
            _FechaModificacion = fechaModificacion;
            _CreadorPor = creadorPor;
            _ModificadoPor = modificadoPor;
        }

        public int Id { get => _Id; set => _Id = value; }
        public int UsuarioReportaId { get => _UsuarioReportaId; set => _UsuarioReportaId = value; }
        public int UsuarioAsignadoId { get => _UsuarioAsignadoId; set => _UsuarioAsignadoId = value; }
        public int Prioridad { get => _Prioridad; set => _Prioridad = value; }
        public int DepartamentoId { get => _DepartamentoId; set => _DepartamentoId = value; }
        public int Titulo { get => _Titulo; set => _Titulo = value; }
        public string Descripcion { get => _Descripcion; set => _Descripcion = value; }
        public string FechaCierre { get => _FechaCierre; set => _FechaCierre = value; }
        public string ComentarioCierre { get => _ComentarioCierre; set => _ComentarioCierre = value; }
        public string Estatus { get => _Estatus; set => _Estatus = value; }
        public string Borrado { get => _Borrado; set => _Borrado = value; }
        public string FechaRegistrado { get => _FechaRegistrado; set => _FechaRegistrado = value; }
        public string FechaModificacion { get => _FechaModificacion; set => _FechaModificacion = value; }
        public string CreadorPor { get => _CreadorPor; set => _CreadorPor = value; }
        public string ModificadoPor { get => _ModificadoPor; set => _ModificadoPor = value; }

        public void AddIncidente(Login login)
        {
            var item = new Conexion();

            int usuarioAsignadoId = 0, prioridadId = 0, departamentoId = 0, lon = 1;
            string titulo = "", description = "", comentario = "", estatus = "";
            do
            {
                string md = "Select * from Usuario where borrado like '0'";
                DataSet s = item.Ejecutar(md);
                DataTable table = null;

                if (s.Tables.Count > 0)
                {
                    table = s.Tables[0];
                }

                Console.WriteLine("Lista de Usuario");
                foreach (DataRow drCurrent in table.Rows)
                {
                    Console.WriteLine("{0}- Nombre: {1}, Apellido: {2}, Cedula: {3}", Convert.ToInt32(drCurrent["id"].ToString()),
                        drCurrent["nombre"].ToString(), drCurrent["apellido"].ToString(), drCurrent["cedula"].ToString());
                }

                Console.Write("\n\t\tElegir el usuario que se encargara del reporte: ");
                try
                {
                    usuarioAsignadoId = Convert.ToInt16(Console.ReadLine());
                    lon = table.Rows.Count;
                }
                catch (Exception e)
                {
                    Console.WriteLine("\n\t\t\tEntra un numero...");
                }
            } while (usuarioAsignadoId < 0);

            do
            {
                Console.Write("\n\t\tElige una Prioridad: ");
                try
                {
                    string md = "Select * from Prioridad";
                    DataSet d = item.Ejecutar(md);
                    DataTable table = null;

                    if (d.Tables.Count > 0)
                    {
                        table = d.Tables[0];
                    }

                    Console.WriteLine("Lista de Prioridades disponibles");
                    foreach (DataRow drCurrent in table.Rows)
                    {
                        Console.WriteLine("{0}- Nombre: {1}", Convert.ToInt32(drCurrent["id"].ToString()),
                            drCurrent["nombre"].ToString());
                    }
                    prioridadId = Convert.ToInt16(Console.ReadLine());
                    lon = table.Rows.Count;
                }
                catch (Exception e)
                {
                    Console.WriteLine("\n\t\t\tEntra un numero...");
                }
            } while (prioridadId < 0);
            
            do
            {
                Console.Write("\n\t\tElige un departamento: ");
                try
                {
                    string md = "Select * from Departamento where borrado like '0'";
                    DataSet d = item.Ejecutar(md);
                    DataTable table = null;

                    if (d.Tables.Count > 0)
                    {
                        table = d.Tables[0];
                    }

                    Console.WriteLine("Lista de departamentos disponibles");
                    foreach (DataRow drCurrent in table.Rows)
                    {
                        Console.WriteLine("{0}- Nombre: {1}", Convert.ToInt32(drCurrent["id"].ToString()),
                            drCurrent["nombre"].ToString());
                    }
                    departamentoId = Convert.ToInt16(Console.ReadLine());
                    lon = table.Rows.Count;
                }
                catch (Exception e)
                {
                    Console.WriteLine("\n\t\t\tEntra un numero...");
                }
            } while (departamentoId < 0);

            Console.Write("\n\t\tDigite un titulo: ");
            titulo = Console.ReadLine();
            Console.Write("\n\t\tDigite una descripcion: ");
            description = Console.ReadLine();
            Console.Write("\n\t\tDigite un comentario: ");
            comentario = Console.ReadLine();
            Console.Write("\n\t\tDigite el estatus: ");
            estatus = Console.ReadLine();

            string cmd = string.Format("Exec spInsertIncidente '{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}'",
               login.Id, usuarioAsignadoId, prioridadId, departamentoId, titulo, description, DateTime.Now, comentario, estatus, login.Id);
            DataSet ds = item.Ejecutar(cmd);

            Console.Clear();
            Console.WriteLine("Se agregó un Incidente");
            Console.WriteLine("Presione una tecla para continuar..");
            Console.ReadKey();
            Console.Clear();
        }

        public void Update(Login login)
        {
            var item = new Conexion();

            int id = 0, usuarioAsignadoId = 0, prioridadId = 0, departamentoId = 0, lon = 1;
            string titulo = "", description = "", comentario = "", estatus = "";
            do
            {
                string md = "Select * from Incidente where borrado like '0'";
                string mdd = "Select * from Incidente ";
                DataSet d = item.Ejecutar(md);
                DataSet dc = item.Ejecutar(mdd);
                DataTable table = null;
                DataTable table2 = null;

                if (d.Tables.Count > 0)
                {
                    table = d.Tables[0];
                    table2 = dc.Tables[0];
                    lon = table2.Rows.Count;
                }

                Console.WriteLine("Lista de Incidentes");
                foreach (DataRow drCurrent in table.Rows)
                {
                    Console.WriteLine("{0}- Titulo: {1}, Description: {2}, Comentario: {3}", Convert.ToInt32(drCurrent["id"].ToString()),
                        drCurrent["titulo"].ToString(), drCurrent["descripcion"].ToString(), drCurrent["comentarioCierre"].ToString());
                }

                Console.Write("\n\t\tElegir el incidente: ");
                try
                {
                    id = Convert.ToInt16(Console.ReadLine());
                }
                catch (Exception e)
                {
                    Console.WriteLine("\n\t\t\tEntra un numero...");
                }
            } while (id < 0);
           
            do
            {
                string md = "Select * from Usuario where borrado like '0'";
                DataSet s = item.Ejecutar(md);
                DataTable table = null;

                if (s.Tables.Count > 0)
                {
                    table = s.Tables[0];
                }
       
                Console.WriteLine("Lista de Usuario");
                foreach (DataRow drCurrent in table.Rows)
                {
                    Console.WriteLine("{0}- Nombre: {1}, Apellido: {2}, Cedula: {3}", Convert.ToInt32(drCurrent["id"].ToString()),
                        drCurrent["nombre"].ToString(), drCurrent["apellido"].ToString(), drCurrent["cedula"].ToString());
                }
               
                Console.Write("\n\t\tElegir el usuario que se encargara del reporte: ");
                try
                {
                    usuarioAsignadoId = Convert.ToInt16(Console.ReadLine());
                    lon = table.Rows.Count;
                }
                catch (Exception e)
                {
                    Console.WriteLine("\n\t\t\tEntra un numero...");
                }
            } while (usuarioAsignadoId < 0);
           
            do
            {
                Console.Write("\n\t\tElige una Prioridad: ");
                try
                {
                    string md = "Select * from Prioridad where borrado like '0'";
                    DataSet d = item.Ejecutar(md);
                    DataTable table = null;

                    if (d.Tables.Count > 0)
                    {
                        table = d.Tables[0];
                    }

                    Console.WriteLine("Lista de Prioridades disponibles");
                    foreach (DataRow drCurrent in table.Rows)
                    {
                        Console.WriteLine("{0}- Nombre: {1}", Convert.ToInt32(drCurrent["id"].ToString()),
                            drCurrent["nombre"].ToString());
                    }
                    prioridadId = Convert.ToInt16(Console.ReadLine());
                    lon = table.Rows.Count;
                }
                catch (Exception e)
                {
                    Console.WriteLine("\n\t\t\tEntra un numero...");
                }
            } while (prioridadId < 0);
            
            do
            {
                Console.Write("\n\t\tElige un departamento: ");
                try
                {
                    string md = "Select * from Departamento where borrado like '0'";
                    DataSet d = item.Ejecutar(md);
                    DataTable table = null;

                    if (d.Tables.Count > 0)
                    {
                        table = d.Tables[0];
                    }

                    Console.WriteLine("Lista de departamentos disponibles");
                    foreach (DataRow drCurrent in table.Rows)
                    {
                        Console.WriteLine("{0}- Nombre: {1}", Convert.ToInt32(drCurrent["id"].ToString()),
                            drCurrent["nombre"].ToString());
                    }
                    departamentoId = Convert.ToInt16(Console.ReadLine());
                    lon = table.Rows.Count;
                }
                catch (Exception e)
                {
                    Console.WriteLine("\n\t\t\tEntra un numero...");
                }
            } while (departamentoId < 0);
            
            Console.Write("\n\t\tDigite un titulo: ");
            titulo = Console.ReadLine();
            Console.Write("\n\t\tDigite una descripcion: ");
            description = Console.ReadLine();
            Console.Write("\n\t\tDigite un comentario: ");
            comentario = Console.ReadLine();
            Console.Write("\n\t\tDigite el estatus: ");
            estatus = Console.ReadLine();

            string cmd = string.Format("Exec spUpdateIncidente '{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}'",
               id, login.Id, usuarioAsignadoId, prioridadId, departamentoId, titulo, description, DateTime.Now, comentario, estatus, DateTime.Now, login.Id);
            DataSet ds = item.Ejecutar(cmd);

            Console.Clear();
            Console.WriteLine("Se actualizó el Incidente");
            Console.WriteLine("Presione una tecla para continuar..");
            Console.ReadKey();
            Console.Clear();
        }

        public void Retrive()
        {
            var item = new Conexion();
            /* string md = "Select * from Incidente where borrado like '0'";
             DataSet s = item.Ejecutar(md);
             DataTable table = null;

             if (s.Tables.Count > 0)
             {
                 table = s.Tables[0];
             }
             */
            string md = "Select * from Incidente where borrado like '0'";
            string mdd = "Select * from Incidente ";
            DataSet d = item.Ejecutar(md);
            DataSet dc = item.Ejecutar(mdd);
            DataTable table = null;
            DataTable table2 = null;

            if (d.Tables.Count > 0)
            {
                table = d.Tables[0];
                table2 = dc.Tables[0];
            }
            Console.WriteLine("Lista de Incidentes");
            foreach (DataRow drCurrent in table.Rows)
            {             
                Console.WriteLine("{0}- Titulo: {1}, Description: {2}, Comentario: {3}", Convert.ToInt32(drCurrent["id"].ToString()),
                    drCurrent["titulo"].ToString(), drCurrent["descripcion"].ToString(), drCurrent["comentarioCierre"].ToString());
            }

            Console.WriteLine("");
            Console.WriteLine("Presione una tecla para continuar..");
            Console.ReadKey();
            Console.Clear();
        }

        public void Delete()
        {
            var item = new Conexion();
            int id = 0, lon = 1;
            do
            {
                string md = "Select * from Incidente where borrado like '0'";
                DataSet s = item.Ejecutar(md);
                DataTable table = null;

                if (s.Tables.Count > 0)
                {
                    table = s.Tables[0];
                }

                Console.WriteLine("Lista de Incidentes");
                foreach (DataRow drCurrent in table.Rows)
                {
                    Console.WriteLine("{0}- Titulo: {1}, Description: {2}, Comentario: {3}", Convert.ToInt32(drCurrent["id"].ToString()),
                        drCurrent["titulo"].ToString(), drCurrent["descripcion"].ToString(), drCurrent["comentarioCierre"].ToString());
                }

                Console.Write("\n\t\tElegir el incidente: ");
                try
                {
                    id = Convert.ToInt16(Console.ReadLine());
                    lon = table.Rows.Count;
                }
                catch (Exception e)
                {
                    Console.WriteLine("\n\t\t\tEntra un numero...");
                }
            } while (id < 0 || id > lon);

            string cmd = string.Format("Exec SpDeleteIncidente '{0}','{1}'",
                id, 1);
            DataSet ds = item.Ejecutar(cmd);

            Console.Clear();
            Console.WriteLine("Se eliminó el incidente");
            Console.WriteLine("Presione una tecla para continuar..");
            Console.ReadKey();
            Console.Clear();
        }
    }
}