﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoFinal
{
    class Program
    {
        static int Main(string[] args)
        {

            int opc = 0;
            var login = new Login();
            var obj = login.SignIn();

            if (obj.Correo != null
                )
            {
                do
                {
                    do
                    {
                        Console.Clear();
                        Console.WriteLine("\n\t\t\t   Menu Principal");
                        Console.WriteLine("\n\t\t\t");
                        Console.WriteLine("\n\t\t\t1- Crud Departamento");
                        Console.WriteLine("\t\t\t2- Crud Puesto");
                        Console.WriteLine("\t\t\t3- Crud User");
                        Console.WriteLine("\t\t\t4- Crud Sla");
                        Console.WriteLine("\t\t\t5- Crud Prioridad");
                        Console.WriteLine("\t\t\t6- Crud Incidente");
                        Console.WriteLine("\t\t\t7- Crud Historial Incidente");
                        Console.WriteLine("\t\t\t8- Salir");
                        Console.Write("\n\t\t\t Elige su opcion: ");
                        try
                        {
                            opc = Convert.ToInt16(Console.ReadLine());
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("\n\t\t\tEntra un numero...");
                        }

                    } while (opc < 1 || opc > 8);

                    switch (opc)
                    {
                        case 1:
                            Console.Clear();
                            CrudDepartamento(obj);
                            break;
                        case 2:
                            Console.Clear();
                            CrudPuesto(obj);
                            break;
                        case 3:
                            Console.Clear();
                            CrudUser(obj);
                            break;
                        case 4:
                            Console.Clear();
                            CrudSla(obj);
                            break;
                        case 5:
                            Console.Clear();
                            CrudPrioridad(obj);
                            break;
                        case 6:
                            Console.Clear();
                            CrudIncidente(obj);
                            break;
                        case 7:
                            Console.Clear();
                            CrudHistorial(obj);
                            break;
                        default:
                            break;
                    }
                } while (opc != 8);
                Console.WriteLine("\n\t\t\tHasta luego... Que Vuelva pronto!!!");
                Console.ReadKey();
            }

            else
                Console.WriteLine("\n\t\t\tTus informaciones tienen errores !!");

            return 0;
        }

        private static void CrudHistorial(Login login)
        {
            var incidente = new HistorialIncidente();
            int opc = -1;
            var item = new Conexion();

            do
            {
                Console.Clear();
                Console.WriteLine("\n\t\t\t   Menu Historial");
                Console.WriteLine("\n\t\t\t");
                Console.WriteLine("\n\t\t\t1- Crear Historial");
                Console.WriteLine("\t\t\t2- Imprimir Historial");
                Console.WriteLine("\n\t\t\t3- Update Historial");
                Console.WriteLine("\t\t\t4- delete Historial");
                Console.WriteLine("\t\t\t5- Regresar");
                Console.Write("\n\t\t\t Elige su opcion: ");

                try
                {
                    opc = Convert.ToInt16(Console.ReadLine());
                }
                catch (Exception e)
                {
                    Console.WriteLine("\n\t\t\tEntra un numero...");
                }

            } while (opc < 1 || opc > 5);

            switch (opc)
            {
                case 1:
                    Console.Clear();
                    incidente.add(login);
                    break;
                case 2:
                    Console.Clear();
                    incidente.Retrieve();
                    break;
                case 3:
                    Console.Clear();
                    incidente.update(login);
                    break;
                case 4:
                    Console.Clear();
                    incidente.Delete();
                    break;
                default:
                    break;
            }
        }

        private static void CrudIncidente(Login login)
        {
            var incidente = new Incidente();
            int opc = -1;
            var item = new Conexion();

            do
            {
                Console.Clear();
                Console.WriteLine("\n\t\t\t   Menu Incidente");
                Console.WriteLine("\n\t\t\t");
                Console.WriteLine("\n\t\t\t1- Crear Incidente");
                Console.WriteLine("\t\t\t2- Imprimir Incidente");
                Console.WriteLine("\t\t\t3- Actualizar Incidente");
                Console.WriteLine("\t\t\t4- Delete Incidente");
                Console.WriteLine("\t\t\t5- Regresar");
                Console.Write("\n\t\t\t Elige su opcion: ");

                try
                {
                    opc = Convert.ToInt16(Console.ReadLine());
                }
                catch (Exception e)
                {
                    Console.WriteLine("\n\t\t\tEntra un numero...");
                }

            } while (opc < 1 || opc > 5);

            switch (opc)
            {
                case 1:
                    Console.Clear();
                    incidente.AddIncidente(login);
                    break;
                case 2:
                    Console.Clear();
                    incidente.Retrive();
                    break;
                case 3:
                    Console.Clear();
                    incidente.Update(login);
                    break;
                case 4:
                    Console.Clear();
                    incidente.Delete();
                    break;
                default:
                    break;
            }
        }

        private static void CrudPrioridad(Login login)
        {
            var prioridad = new Prioridad();
            int opc = -1;
            var item = new Conexion();

            do
            {
                Console.Clear();
                Console.WriteLine("\n\t\t\t   Menu Prioridad");
                Console.WriteLine("\n\t\t\t");
                Console.WriteLine("\n\t\t\t1- Crear prioridad");
                Console.WriteLine("\t\t\t2- Imprimir prioridad");
                Console.WriteLine("\t\t\t3- Actualizar prioridad");
                Console.WriteLine("\t\t\t4- Delete prioridad");
                Console.WriteLine("\t\t\t5- Regresar");
                Console.Write("\n\t\t\t Elige su opcion: ");

                try
                {
                    opc = Convert.ToInt16(Console.ReadLine());
                }
                catch (Exception e)
                {
                    Console.WriteLine("\n\t\t\tEntra un numero...");
                }

            } while (opc < 1 || opc > 5);

            switch (opc)
            {
                case 1:
                    Console.Clear();
                    prioridad.AddPrioridad(login);
                    break;
                case 2:
                    Console.Clear();
                    prioridad.Retrieve();
                    break;
                case 3:
                    Console.Clear();
                    prioridad.UpdatePrioridad(login);
                    break;
                case 4:
                    Console.Clear();
                    prioridad.DeletePrioridad();
                    break;
                default:
                    break;
            }
        }

        private static void CrudSla(Login login)
        {
            var sla = new Sla();
            int opc = -1;
            var item = new Conexion();

            do
            {
                Console.Clear();
                Console.WriteLine("\n\t\t\t   Menu Sla");
                Console.WriteLine("\n\t\t\t");
                Console.WriteLine("\n\t\t\t1- Crear Sla");
                Console.WriteLine("\t\t\t2- Imprimir Sla");
                Console.WriteLine("\t\t\t3- Actualizar Sla");
                Console.WriteLine("\t\t\t4- Delete Sla");
                Console.WriteLine("\t\t\t5- Regresar");
                Console.Write("\n\t\t\t Elige su opcion: ");

                try
                {
                    opc = Convert.ToInt16(Console.ReadLine());
                }
                catch (Exception e)
                {
                    Console.WriteLine("\n\t\t\tEntra un numero...");
                }

            } while (opc < 1 || opc > 5);


            switch (opc)
            {
                case 1:
                    Console.Clear();
                    sla.addSla(login);
                    break;
                case 2:
                    Console.Clear();
                    sla.retrieve();
                    break;
                case 3:
                    Console.Clear(); 
                    sla.updateSla(login);
                    break;
                case 4:
                    Console.Clear();
                    sla.deleteSla();
                    break;
                default:
                    break;
            }
        }

        private static void CrudPuesto(Login login)
        {

            var puesto = new Puesto();
            int opc = -1;
            List<Usuario> listaUsuario = new List<Usuario>();
            var item = new Conexion();

            do
            {
                Console.Clear();
                Console.WriteLine("\n\t\t\t   Menu Puesto");
                Console.WriteLine("\n\t\t\t");
                Console.WriteLine("\n\t\t\t1- Crear Puesto");
                Console.WriteLine("\t\t\t2- Imprimir Puesto");
                Console.WriteLine("\t\t\t3- Actualizar Puesto");
                Console.WriteLine("\t\t\t4- Delete Puesto");
                Console.WriteLine("\t\t\t5- Regresar");
                Console.Write("\n\t\t\t Elige su opcion: ");

                try
                {
                    opc = Convert.ToInt16(Console.ReadLine());
                }
                catch (Exception e)
                {
                    Console.WriteLine("\n\t\t\tEntra un numero...");
                }

            } while (opc < 1 || opc > 5);

            switch (opc)
            {
                case 1:
                    Console.Clear();
                    puesto.AddPuesto(login.Id);
                    break;
                case 2:
                    Console.Clear();
                    puesto.Retrieve();
                    break;
                case 3:
                    Console.Clear();
                    puesto.UpdatePuesto(login.Id);
                    break;
                case 4:
                    Console.Clear();
                    puesto.DeletePuesto();
                    break;
                default:
                    break;
            }
        }

        private static void CrudDepartamento(Login login)
        {

            var depart = new Departamento();
            int opc = -1;
            List<Usuario> listaUsuario = new List<Usuario>();
            var item = new Conexion();
        
            do
            {
                Console.Clear();
                Console.WriteLine("\n\t\t\t   Menu Departamento");
                Console.WriteLine("\n\t\t\t");
                Console.WriteLine("\n\t\t\t1- Crear Departamento");
                Console.WriteLine("\t\t\t2- Imprimir Departamento");
                Console.WriteLine("\t\t\t3- Actualizar Departamento");
                Console.WriteLine("\t\t\t4- Delete Departamento");
                Console.WriteLine("\t\t\t5- Regresar");
                Console.Write("\n\t\t\t Elige su opcion: ");

                try
                {
                    opc = Convert.ToInt16(Console.ReadLine());
                }
                catch (Exception e)
                {
                    Console.WriteLine("\n\t\t\tEntra un numero...");
                }
            } while (opc < 1 || opc > 5);

            switch (opc)
            {
                case 1:
                    Console.Clear();
                    depart.AddDepartment(login.Id);
                    break;
                case 2:
                    Console.Clear();
                    depart.RetrieveDepartment();
                    break;
                case 3:
                    Console.Clear();
                    depart.UpdateDepartment(login.Id);
                    break;
                case 4:
                    Console.Clear();
                    depart.DeleteDepartment();
                    break;
                default:
                    break;
            }
        }

        private static void CrudUser(Login login)
        {
            int opc = -1;
            List<Usuario> listaUsuario = new List<Usuario>();
            var item = new Conexion();
         
            do
            {
                Console.Clear();
                Console.WriteLine("\n\t\t\t   Menu Usuario");
                Console.WriteLine("\n\t\t\t");
                Console.WriteLine("\n\t\t\t1- Crear Usuario");
                Console.WriteLine("\t\t\t2- Imprimir Usuario");
                Console.WriteLine("\t\t\t3- Actualizar usuario");
                Console.WriteLine("\t\t\t4- Delete Usuario");
                Console.WriteLine("\t\t\t5- Regresar");
                Console.Write("\n\t\t\t Elige su opcion: ");

                try
                {
                    opc = Convert.ToInt16(Console.ReadLine());
                }
                catch (Exception e)
                {
                    Console.WriteLine("\n\t\t\tEntra un numero...");
                }

            } while (opc < 1 || opc > 5);

            switch (opc)
            {
                case 1:
                    Console.Clear();
                    CreateUser(login.Id);
                    break;
                case 2:
                    Console.Clear();
                    RetrieveUser();
                    break;
                case 3:
                    Console.Clear();
                    UpdateUser(login.Id);
                    break;
                case 4:
                    Console.Clear();
                    deleteUser();
                    break;
                default:
                    break;
            }
        }

        private static void CreateUser(int idUser)
        {
            var item = new Conexion();
            string nombre = "", apellido = "", cedula = "", correo = "", nombreUsuario = "", contrasena = "", estatus = "";
            int puestoId = 0, lon = 1;
            Console.Write("\n\t\tDigite el nombre del usuario: ");
            nombre = Console.ReadLine();
            Console.Write("\n\t\tDigite el apellido del usuario: ");
            apellido = Console.ReadLine();
            Console.Write("\n\t\tDigite la cedula del usuario: ");
            cedula = Console.ReadLine();
            Console.Write("\n\t\tDigite el correo del usuario: ");
            correo = Console.ReadLine();
            Console.Write("\n\t\tDigite el nombre de usuario del usuario: ");
            nombreUsuario = Console.ReadLine();
            Console.Write("\n\t\tDigite la contrasena del usuario: ");
            contrasena = Console.ReadLine();
            Console.Write("\n\t\tDigite el estatus del usuario: ");
            estatus = Console.ReadLine();

            do
            {
                Console.Write("\n\t\tDigite el puesto del usuario: ");
                try
                {
                    string cmd = "Select * from Puesto where borrado like '0'";
                    DataSet ds = item.Ejecutar(cmd);
                    DataTable table = null;

                    if (ds.Tables.Count > 0)
                    {
                        table = ds.Tables[0];
                    }

                    Console.WriteLine("Lista de Puesto disponibles");
                    foreach (DataRow drCurrent in table.Rows)
                    {
                        Console.WriteLine("{0}- Nombre: {1}", Convert.ToInt32(drCurrent["id"].ToString()),
                            drCurrent["nombre"].ToString());
                    }
                    puestoId = Convert.ToInt16(Console.ReadLine());
                    lon = table.Rows.Count;
                }
                catch (Exception e)
                {
                    Console.WriteLine("\n\t\t\tEntra un numero...");
                }
            } while (puestoId < 0);

            Usuario obj = new Usuario(idUser, puestoId, nombre, apellido, cedula, correo, nombreUsuario, contrasena, estatus);
            Usuario usuario = new Usuario();
            usuario.AddUser(obj);

            Console.Clear();
            Console.WriteLine("Se añadió un Usuario");
            Console.WriteLine("Presione una tecla para continuar..");
            Console.ReadKey();
            Console.Clear();
        }

        private static void RetrieveUser()
        {
            var item = new Conexion();
            string cmd = "Select * from Usuario";
            DataSet ds = item.Ejecutar(cmd);
            DataTable table = null;

            if (ds.Tables.Count > 0)
            {
                table = ds.Tables[0];
            }

            Console.WriteLine("Lista de Usuario");
            Console.WriteLine("");
            foreach (DataRow drCurrent in table.Rows)
            {
                Console.WriteLine("{0}- Nombre: {1},\n Apellido: {2},\n Cedula: {3},\n PuestoID: {4},\n Modificado Por: {5},\n Creado Por: {6}\n=========================\n", Convert.ToInt32(drCurrent["id"].ToString()),
                    drCurrent["nombre"].ToString(), drCurrent["apellido"].ToString(), drCurrent["cedula"].ToString(), drCurrent["puestoId"].ToString(),
                    drCurrent["modificadoPor"].ToString(), drCurrent["creadoPor"].ToString());
            }
            Console.WriteLine("");
            Console.WriteLine("Presione una tecla para continuar..");
            Console.ReadKey();
            Console.Clear();
        }

        private static void UpdateUser(int userlogin)
        {
            var item = new Conexion();
            string nombre = "", apellido = "", cedula = "", correo = "", nombreUsuario = "", contrasena = "", estatus = "";
            int puestoId = 0, id = 0, lon = 0;
            do
            {
                string cmd = "Select * from Usuario";
                DataSet ds = item.Ejecutar(cmd);
                DataTable table = null;

                if (ds.Tables.Count > 0)
                {
                    table = ds.Tables[0];
                }

                Console.WriteLine("Lista de Usuario");
                foreach (DataRow drCurrent in table.Rows)
                {
                    Console.WriteLine("{0}- Nombre: {1}, Apellido: {2}, Cedula: {3}", Convert.ToInt32(drCurrent["id"].ToString()),
                        drCurrent["nombre"].ToString(), drCurrent["apellido"].ToString(), drCurrent["cedula"].ToString());
                }

                Console.Write("\n\t\tElegir el usuario: ");
                try
                {
                    id = Convert.ToInt16(Console.ReadLine());
                    lon = table.Rows.Count;
                }
                catch (Exception e)
                {
                    Console.WriteLine("\n\t\t\tEntra un numero...");
                }
            } while (id < 0);

            Console.Write("\n\t\tDigite el nombre del usuario: ");
            nombre = Console.ReadLine();
            Console.Write("\n\t\tDigite el apellido del usuario: ");
            apellido = Console.ReadLine();
            Console.Write("\n\t\tDigite la cedula del usuario: ");
            cedula = Console.ReadLine();
            Console.Write("\n\t\tDigite el correo del usuario: ");
            correo = Console.ReadLine();
            Console.Write("\n\t\tDigite el nombre de usuario del usuario: ");
            nombreUsuario = Console.ReadLine();
            Console.Write("\n\t\tDigite la contrasena del usuario: ");
            contrasena = Console.ReadLine();
            Console.Write("\n\t\tDigite el estatus del usuario: ");
            estatus = Console.ReadLine();
            do
            {
                Console.Write("\n\t\tDigite el puesto del usuario: ");
                try
                {
                    string cmd = "Select * from Puesto  where borrado = '0'";
                    DataSet ds = item.Ejecutar(cmd);
                    DataTable table = null;

                    if (ds.Tables.Count > 0)
                    {
                        table = ds.Tables[0];
                    }

                    Console.WriteLine("Lista de Puesto disponibles");
                    foreach (DataRow drCurrent in table.Rows)
                    {
                        Console.WriteLine("{0}- Nombre: {1}", Convert.ToInt32(drCurrent["id"].ToString()),
                            drCurrent["nombre"].ToString());
                    }
                    puestoId = Convert.ToInt16(Console.ReadLine());
                    lon = table.Rows.Count;
                }
                catch (Exception e)
                {
                    Console.WriteLine("\n\t\t\tEntra un numero...");
                }
            } while (puestoId < 0);

            Usuario obj = new Usuario(id, puestoId, nombre, apellido, cedula, correo, nombreUsuario, contrasena, estatus);
            Usuario usuario = new Usuario();
            usuario.UpdateUser(obj, Convert.ToString(DateTime.Now), userlogin);

            Console.Clear();
            Console.WriteLine("Se actualizó el Usuario");
            Console.WriteLine("Presione una tecla para continuar..");
            Console.ReadKey();
            Console.Clear();
        }

        private static void deleteUser()
        {
            var item = new Conexion();
            int borrado = 0, id = 0, lon = 0;
            do
            {
                string cmd = "Select * from Usuario where borrado like '0'";
                DataSet ds = item.Ejecutar(cmd);
                DataTable table = null;

                if (ds.Tables.Count > 0)
                {
                    table = ds.Tables[0];
                }

                Console.WriteLine("Lista de Usuario");
                foreach (DataRow drCurrent in table.Rows)
                {
                    Console.WriteLine("{0}- Nombre: {1}, Apellido: {2}, Cedula: {3}", Convert.ToInt32(drCurrent["id"].ToString()),
                        drCurrent["nombre"].ToString(), drCurrent["apellido"].ToString(), drCurrent["cedula"].ToString());
                }

                Console.Write("\n\t\tElegir el usuario: ");
                try
                {
                    id = Convert.ToInt16(Console.ReadLine());
                    lon = table.Rows.Count;
                }
                catch (Exception e)
                {
                    Console.WriteLine("\n\t\t\tEntra un numero...");
                }
            } while (id < 0 || id > lon);

            Usuario usuario = new Usuario();
            usuario.DeleteUser(id, 1);

            Console.Clear();
            Console.WriteLine("Se eliminó el Usuario");
            Console.WriteLine("Presione una tecla para continuar..");
            Console.ReadKey();
            Console.Clear();
        }
    }
}
