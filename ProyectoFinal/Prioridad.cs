﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace ProyectoFinal
{
    class Prioridad
    {
        int _Id;
        int _SlaId;
        string _Nombre;
        string _Estatus;
        string _FechaModificacion;
        string _CreadorPor;
        string _ModificadoPor;

        public Prioridad()
        {

        }

        public Prioridad(int id, int slaId, string nombre, string estatus, string fechaModificacion, string creadorPor, string modificadoPor)
        {
            Id = id;
            SlaId = slaId;
            Nombre = nombre;
            Estatus = estatus;
            FechaModificacion = fechaModificacion;
            CreadorPor = creadorPor;
            ModificadoPor = modificadoPor;
        }

        public int Id { get => _Id; set => _Id = value; }
        public int SlaId { get => _SlaId; set => _SlaId = value; }
        public string Nombre { get => _Nombre; set => _Nombre = value; }
        public string Estatus { get => _Estatus; set => _Estatus = value; }
        public string FechaModificacion { get => _FechaModificacion; set => _FechaModificacion = value; }
        public string CreadorPor { get => _CreadorPor; set => _CreadorPor = value; }
        public string ModificadoPor { get => _ModificadoPor; set => _ModificadoPor = value; }

        public void AddPrioridad(Login login)
        {
            var item = new Conexion();
            string nombre = "", estatus = "";
            int id = 0, lon = 1;

            Console.Write("\n\t\tDigite el nombre de la prioridad: ");
            nombre = Console.ReadLine();
            Console.Write("\n\t\tDigite el estatus de la prioridad: ");
            estatus = Console.ReadLine();
           
            do
            {
                Console.Write("\n\t\tElige un Sla: ");
                try
                {
                     string md = "Select * from Sla where borrado like '0'";
                string mdd = "Select * from Sla ";
                DataSet d = item.Ejecutar(md);
                DataSet dc = item.Ejecutar(mdd);
                DataTable table = null;
                DataTable table2 = null;

                if (d.Tables.Count > 0)
                {
                    table = d.Tables[0];
                    table2 = dc.Tables[0];
                    lon = table2.Rows.Count;
                }

                    Console.WriteLine("Lista de Sla disponibles");
                    foreach (DataRow drCurrent in table.Rows)
                    {
                        Console.WriteLine("{0}- Descripcion: {1}", Convert.ToInt32(drCurrent["id"].ToString()),
                            drCurrent["descripcion"].ToString());
                    }
                    id = Convert.ToInt16(Console.ReadLine());
                   
                }
                catch (Exception e)
                {
                    Console.WriteLine("\n\t\t\tEntra un numero...");
                }
            } while (id < 0 || id > lon);

            string cmd = string.Format("Exec spInsertPrioridad '{0}','{1}','{2}','{3}'",
                id, nombre, estatus, login.Id);
            DataSet ds = item.Ejecutar(cmd);

            Console.Clear();
            Console.WriteLine("Se agregó una Prioridad");
            Console.WriteLine("Presione una tecla para continuar..");
            Console.ReadKey();
            Console.Clear();
        }

        public void UpdatePrioridad(Login login)
        {
            var item = new Conexion();
            string nombre = "", estatus = "";
            int id = 0, slaId = 0, lon = 1;
           
            do
            {
                Console.Write("\n\t\tElige una Prioridad: ");
                try
                {
                    string md = "Select * from Prioridad where borrado like '0'";
                    string mdd = "Select * from Prioridad ";
                    DataSet d = item.Ejecutar(md);
                    DataSet dc = item.Ejecutar(mdd);
                    DataTable table = null;
                    DataTable table2 = null;

                    if (d.Tables.Count > 0)
                    {
                        table = d.Tables[0];
                        table2 = dc.Tables[0];
                        lon = table2.Rows.Count;
                    }

                    Console.WriteLine("Lista de Prioridades disponibles");
                    foreach (DataRow drCurrent in table.Rows)
                    {
                        Console.WriteLine("{0}- Nombre: {1}", Convert.ToInt32(drCurrent["id"].ToString()),
                            drCurrent["nombre"].ToString());
                    }
                    id = Convert.ToInt16(Console.ReadLine());
                }
                catch (Exception e)
                {
                    Console.WriteLine("\n\t\t\tEntra un numero...");
                }
            } while (id < 0 || id > lon);
           
            do
            {
                Console.Write("\n\t\tElige un Sla: ");
                try
                {
                    string md = "Select * from Sla where borrado like '0'";
                    DataSet d = item.Ejecutar(md);
                    DataTable table = null;

                    if (d.Tables.Count > 0)
                    {
                        table = d.Tables[0];
                    }
                    
                    Console.WriteLine("Lista de Sla disponibles");
                    foreach (DataRow drCurrent in table.Rows)
                    {
                        Console.WriteLine("{0}- Descripcion: {1}", Convert.ToInt32(drCurrent["id"].ToString()),
                            drCurrent["descripcion"].ToString());
                    }
                    slaId = Convert.ToInt16(Console.ReadLine());
                    lon = table.Rows.Count;
                    
                }
                catch (Exception e)
                {
                    Console.WriteLine("\n\t\t\tEntra un numero...");
                }
            } while (slaId < 0 || slaId > lon);
           
            Console.Write("\n\t\tDigite el nombre de la prioridad: ");
            nombre = Console.ReadLine();
            Console.Write("\n\t\tDigite el estatus de la prioridad: ");
            estatus = Console.ReadLine();
            using (SqlConnection conn = new SqlConnection(Conexion.con))
                        {
                            using (SqlCommand cmd = new SqlCommand("SpUpdatePrioridad", conn))
                            {
                                cmd.CommandType = CommandType.StoredProcedure;

                                cmd.Parameters.Add(new SqlParameter("@id", id));
                                cmd.Parameters.Add(new SqlParameter("@slaId", slaId));
                                cmd.Parameters.Add(new SqlParameter("@nombre", nombre));
                                cmd.Parameters.Add(new SqlParameter("@estatus", estatus));
                                cmd.Parameters.Add(new SqlParameter("@fechaModificacion", DateTime.Now));
                                cmd.Parameters.Add(new SqlParameter("@modificadoPor", login.Id));
                                

                                conn.Open();
                                cmd.ExecuteNonQuery();
                            }
                        }
            /* string cmd = string.Format("Exec spUpdatePrioridad '{0}','{1}','{2}','{3}'',{4}','{5}'",
                 id, slaId, nombre, estatus, DateTime.Now, login.Id);
             DataSet ds = item.Ejecutar(cmd);
            */

            Console.Clear();
            Console.WriteLine("Se actualizó la Prioridad");
            Console.WriteLine("Presione una tecla para continuar..");
            Console.ReadKey();
            Console.Clear();
        }

        public void Retrieve()
        {
            var item = new Conexion();
            string md = "Select * from Prioridad where borrado like '0'";
            DataSet d = item.Ejecutar(md);
            DataTable table = null;

            if (d.Tables.Count > 0)
            {
                table = d.Tables[0];
            }

            Console.WriteLine("Lista de prioridades disponibles");
            foreach (DataRow drCurrent in table.Rows)
            {
                Console.WriteLine("{0}- Nombre: {1}", Convert.ToInt32(drCurrent["id"].ToString()),
                    drCurrent["nombre"].ToString());
            }

            Console.WriteLine("");
            Console.WriteLine("Presione una tecla para continuar..");
            Console.ReadKey();
            Console.Clear();
        }

        public void DeletePrioridad()
        {
            var item = new Conexion();
            int id = 0, lon = 1;
            do
            {
                Console.Write("\n\t\tElige una Prioridad: ");
                
                try
                {
                    string md = "Select * from Prioridad where borrado like '0'";
                    DataSet d = item.Ejecutar(md);
                    DataTable table = null;

                    if (d.Tables.Count > 0)
                    {
                        table = d.Tables[0];
                    }

                    Console.WriteLine("Lista de Prioridades disponibles");
                    foreach (DataRow drCurrent in table.Rows)
                    {
                        Console.WriteLine("{0}- Nombre: {1}", Convert.ToInt32(drCurrent["id"].ToString()),
                            drCurrent["nombre"].ToString());
                    }
                    id = Convert.ToInt16(Console.ReadLine());
                    lon = table.Rows.Count;
                }
                catch (Exception e)
                {
                    Console.WriteLine("\n\t\t\tEntra un numero...");
                }
            } while (id < 0 || id > lon);

            string cmd = string.Format("Exec spDeletePrioridad '{0}','{1}'",
                id, 1);
            DataSet ds = item.Ejecutar(cmd);

            Console.Clear();
            Console.WriteLine("Se eliminó la Prioridad");
            Console.WriteLine("Presione una tecla para continuar..");
            Console.ReadKey();
            Console.Clear();
        }
    }
}
