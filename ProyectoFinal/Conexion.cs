﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace ProyectoFinal
{
    public class Conexion
    {
        //Cadena de conexion de Jhon "Data Source=DESKTOP-G5UJ4TK;Initial Catalog=Finald;Persist Security Info=True;User ID=sa;Password=123456"
        //Cadena de conexion de Anibal "Data Source=DESKTOP-O776D9S\SQLEXPRESS01;Initial Catalog=Finald;Integrated Security=True"

        public static string con = "Data Source=DESKTOP-G5UJ4TK;Initial Catalog=Finald;Persist Security Info=True;User ID=sa;Password=123456";
        SqlConnection sqlCon = new SqlConnection();
        SqlDataAdapter adap;

        public Conexion()
        {
            sqlCon.ConnectionString = con;
        }

        public void Abrir()
        {
            try
            {
                sqlCon.Open();
                //  MessageBox.Show("conectó...");

            }
            catch (Exception ex)
            {
                Console.WriteLine("No se conectó...");
            }
        }
        //Cerrar la conexion
        public void Cerrar()
        {
            sqlCon.Close();
        }

        //Nos permite ejcutar query
        public DataSet Ejecutar(string cadena)
        {
            DataSet resultado = new DataSet();
            try
            {
                Abrir();
                adap = new SqlDataAdapter("" + cadena, con);
                adap.Fill(resultado);
            }
            catch (Exception ex)
            {
                resultado = null;
            }
            finally
            {
                if (sqlCon.State == ConnectionState.Open) Cerrar();
            }

            return resultado;
        }

    }
}
