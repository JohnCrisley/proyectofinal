﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace ProyectoFinal
{
    class Puesto
    {
        int _Id;
        int _DepartamentoId;
        string _Nombre;
        string _Estatus;
        public Puesto()
        {

        }

        public Puesto(int id, int departamentoId, string nombre, string estatus)
        {
            Id = id;
            DepartamentoId = departamentoId;
            Nombre = nombre;
            Estatus = estatus;
        }

        public int Id { get => _Id; set => _Id = value; }
        public int DepartamentoId { get => _DepartamentoId; set => _DepartamentoId = value; }
        public string Nombre { get => _Nombre; set => _Nombre = value; }
        public string Estatus { get => _Estatus; set => _Estatus = value; }


        public void AddPuesto(int idUser)
        {
            var item = new Conexion();
            string nombre = "", estatus = "";
            int departmentId = 0, lon = 0;

            do
            {
                Console.Write("\n\t\tElige del un departamento: ");
                try
                {
                    string md = "Select * from Departamento where borrado like '0'";
                    DataSet d = item.Ejecutar(md);
                    DataTable table = null;

                    if (d.Tables.Count > 0)
                    {
                        table = d.Tables[0];
                    }

                    Console.WriteLine("Lista de Departamento disponibles");
                    foreach (DataRow drCurrent in table.Rows)
                    {
                        Console.WriteLine("{0}- Nombre: {1}", Convert.ToInt32(drCurrent["id"].ToString()),
                            drCurrent["nombre"].ToString());
                    }
                    departmentId = Convert.ToInt16(Console.ReadLine());
                    lon = table.Rows.Count;
                }
                catch (Exception e)
                {
                    Console.WriteLine("\n\t\t\tEntra un numero...");
                }
            } while (departmentId < 0);

            Console.Write("\n\t\tDigite el nombre del puesto: ");
            nombre = Console.ReadLine();
            Console.Write("\n\t\tDigite el estatus del puesto: ");
            estatus = Console.ReadLine();

            var obj = new Puesto();
            obj.DepartamentoId = departmentId;
            obj.Nombre = nombre;
            obj.Estatus = estatus;

            string cmd = string.Format("Exec spInsertPuesto '{0}','{1}','{2}','{3}'",
                obj.DepartamentoId, obj.Nombre, obj.Estatus, idUser);
            DataSet ds = item.Ejecutar(cmd);

            Console.Clear();
            Console.WriteLine("Se añadió un Puesto");
            Console.WriteLine("Presione una tecla para continuar..");
            Console.ReadKey();
            Console.Clear();
        }


        public void Retrieve()
        {
            var item = new Conexion();
            string md = "Select * from Puesto where borrado like '0'";
            DataSet d = item.Ejecutar(md);
            DataTable table = null;

            if (d.Tables.Count > 0)
            {
                table = d.Tables[0];
            }
            Console.WriteLine("Lista de puestos disponibles");
            foreach (DataRow drCurrent in table.Rows)
            {
                Console.WriteLine("{0}- Nombre: {1}", Convert.ToInt32(drCurrent["id"].ToString()),
                    drCurrent["nombre"].ToString());
            }

            Console.WriteLine("");
            Console.WriteLine("Presione una tecla para continuar..");
            Console.ReadKey();
            Console.Clear();
        }

        public void UpdatePuesto(int idUser)
        {
            var item = new Conexion();
            string nombre = "", estatus = "";
            int departmentId = 0, lon = 1, id = 0;
            DateTime fechaModificacion = DateTime.Now;

            do
            {
                Console.Write("\n\t\tDigite el puesto: ");
                try
                {
                    string md = "Select * from Puesto where borrado like '0'";
                    DataSet d = item.Ejecutar(md);
                    DataTable table = null;

                    if (d.Tables.Count > 0)
                    {
                        table = d.Tables[0];
                    }

                    Console.WriteLine("Lista de Puesto disponibles");
                    foreach (DataRow drCurrent in table.Rows)
                    {
                        Console.WriteLine("{0}- Nombre: {1}", Convert.ToInt32(drCurrent["id"].ToString()),
                            drCurrent["nombre"].ToString());
                    }
                    id = Convert.ToInt16(Console.ReadLine());
                    lon = table.Rows.Count;
                }
                catch (Exception e)
                {
                    Console.WriteLine("\n\t\t\tEntra un numero...");
                }
            } while (id < 0);

            do
            {
                Console.Write("\n\t\tElige del un departamento: ");
                try
                {
                    string md = "Select * from Departamento where borrado like '0'";
                    DataSet d = item.Ejecutar(md);
                    DataTable table = null;

                    if (d.Tables.Count > 0)
                    {
                        table = d.Tables[0];
                    }

                    Console.WriteLine("Lista de Departamento disponibles");
                    foreach (DataRow drCurrent in table.Rows)
                    {
                        Console.WriteLine("{0}- Nombre: {1}", Convert.ToInt32(drCurrent["id"].ToString()),
                            drCurrent["nombre"].ToString());
                    }
                    departmentId = Convert.ToInt16(Console.ReadLine());
                    lon = table.Rows.Count;
                }
                catch (Exception e)
                {
                    Console.WriteLine("\n\t\t\tEntra un numero...");
                }
            } while (departmentId < 0);

            Console.Write("\n\t\tDigite el nombre del puesto: ");
            nombre = Console.ReadLine();
            Console.Write("\n\t\tDigite el estatus del puesto: ");
            estatus = Console.ReadLine();

            var obj = new Puesto();
            obj.Id = id;
            obj.DepartamentoId = departmentId;
            obj.Nombre = nombre;
            obj.Estatus = estatus;
            string cmd = string.Format("Exec spUpdatePuesto '{0}','{1}','{2}','{3}','{4}','{5}'",
                obj.Id, obj.DepartamentoId, obj.Nombre, obj.Estatus, fechaModificacion, idUser);
            DataSet ds = item.Ejecutar(cmd);

            Console.Clear();
            Console.WriteLine("Se actualizó el Puesto");
            Console.WriteLine("Presione una tecla para continuar..");
            Console.ReadKey();
            Console.Clear();
        }

        public void DeletePuesto()
        {
            var item = new Conexion();
            int lon = 1, id = 0;
            do
            {
                Console.Write("\n\t\tDigite el puesto: ");
                try
                {
                    string md = "Select * from Puesto where borrado like '0'";
                    DataSet d = item.Ejecutar(md);
                    DataTable table = null;

                    if (d.Tables.Count > 0)
                    {
                        table = d.Tables[0];
                    }

                    Console.WriteLine("Lista de Puesto disponibles");
                    foreach (DataRow drCurrent in table.Rows)
                    {
                        Console.WriteLine("{0}- Nombre: {1}", Convert.ToInt32(drCurrent["id"].ToString()),
                            drCurrent["nombre"].ToString());
                    }
                    id = Convert.ToInt16(Console.ReadLine());
                    lon = table.Rows.Count;
                }
                catch (Exception e)
                {
                    Console.WriteLine("\n\t\t\tEntra un numero...");
                }
            } while (id < 0);
            string cmd = string.Format("Exec spDeletePuesto '{0}','{1}'",
                id, 1);
            DataSet ds = item.Ejecutar(cmd);

            Console.Clear();
            Console.WriteLine("Se eliminó el Puesto");
            Console.WriteLine("Presione una tecla para continuar..");
            Console.ReadKey();
            Console.Clear();
        }
    }
}