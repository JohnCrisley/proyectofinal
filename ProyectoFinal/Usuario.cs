﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace ProyectoFinal
{
    class Usuario
    {
        int _Id;
        int _PuestoID;
        string _Nombre;
        string _Cedula;
        string _Correo;
        string _NombreUsuario;
        string _Contrasena;
        string _Estatus;
        string _Apellido;
        List<Usuario> listaUsuario = new List<Usuario>();

        public Usuario()
        {

        }

        public Usuario(int id, int puestoID, string nombre, string apellido, string cedula, string correo, string nombreUsuario, string contrasena, string estatus)
        {
            Id = id;
            PuestoID = puestoID;
            Apellido = apellido;
            Nombre = nombre;
            Cedula = cedula;
            Correo = correo;
            NombreUsuario = nombreUsuario;
            Contrasena = contrasena;
            Estatus = estatus;
        }

        public int Id { get => _Id; set => _Id = value; }
        public int PuestoID { get => _PuestoID; set => _PuestoID = value; }
        public string Nombre { get => _Nombre; set => _Nombre = value; }
        public string Cedula { get => _Cedula; set => _Cedula = value; }
        public string Correo { get => _Correo; set => _Correo = value; }
        public string NombreUsuario { get => _NombreUsuario; set => _NombreUsuario = value; }
        public string Contrasena { get => _Contrasena; set => _Contrasena = value; }
        public string Estatus { get => _Estatus; set => _Estatus = value; }
        public string Apellido { get => _Apellido; set => _Apellido = value; }


        public void AddUser(Usuario usuario)
        {
            listaUsuario.Add(usuario);
            var item = new Conexion();
            string cmd = string.Format("Exec spInsertUsuario '{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}'",
                usuario.PuestoID, usuario.Nombre, usuario.Apellido, usuario.Cedula, usuario.Correo, usuario.NombreUsuario, usuario.Contrasena, usuario.Estatus, DateTime.Now, usuario.Id);
            DataSet ds = item.Ejecutar(cmd);
        }

        public void UpdateUser(Usuario usuario, string fechaModificacion, int id)
        {
            var item = new Conexion();
            string cmd = string.Format("Exec spUpdateUsuario '{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}'",
                usuario.Id, usuario.Nombre, usuario.Apellido, usuario.Cedula, usuario.Correo, usuario.NombreUsuario, usuario.Contrasena, usuario.Estatus, DateTime.Now, id, usuario.PuestoID);
            DataSet ds = item.Ejecutar(cmd);
        }

        public void DeleteUser(int id, int borrado)
        {
            var item = new Conexion();
            string cmd = string.Format("Exec spDeleteUsuario '{0}','{1}'",
                id, borrado);
            DataSet ds = item.Ejecutar(cmd);
        }
    }
}
