﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace ProyectoFinal
{
    class Departamento
    {

        int _Id;
        string _Nombre;
        string _Estatus;
        string _FechaModificacion;

        public Departamento()
        {

        }

        public Departamento(int id, string nombre, string estatus, string fechaModificacion)
        {
            Id = id;
            Nombre = nombre;
            Estatus = estatus;
            FechaModificacion = fechaModificacion;
        }

        public int Id { get => _Id; set => _Id = value; }
        public string Nombre { get => _Nombre; set => _Nombre = value; }
        public string Estatus { get => _Estatus; set => _Estatus = value; }
        public string FechaModificacion { get => _FechaModificacion; set => _FechaModificacion = value; }

        public void AddDepartment(int idUser)
        {

            string nombre = "", estatus = "";
            Console.Write("\n\t\tDigite el nombre del departamento: ");
            nombre = Console.ReadLine();
            Console.Write("\n\t\tDigite el estatus del departamento: ");
            estatus = Console.ReadLine();

            var departamento = new Departamento();
            departamento.Nombre = nombre;
            departamento.Estatus = estatus;
            var item = new Conexion();
            using (SqlConnection conn = new SqlConnection(Conexion.con))
            {
                using (SqlCommand cmd = new SqlCommand("SpInsertDepartamento", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("@nombre", nombre));
                    cmd.Parameters.Add(new SqlParameter("@estatus", estatus));
                    cmd.Parameters.Add(new SqlParameter("@creadoPor", idUser));

                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
            /*string cmd = string.Format("Exec spInsertDepartamento '{0}','{1}','{2}'",
                departamento.Nombre, departamento.Estatus, idUser);
            DataSet ds = item.Ejecutar(cmd);
            */

            Console.Clear();
            Console.WriteLine("Se añadió un Departamento");
            Console.WriteLine("Presione una tecla para continuar..");
            Console.ReadKey();
            Console.Clear();
        }

        public void UpdateDepartment(int idUser)
        {
            var item = new Conexion();
            string nombre = "", estatus = "";
            DateTime fechaModificacion = DateTime.Now;
            int id = 0, lon = 1;
            do
            {
                Console.Write("\n\t\tElige del un departamento: ");
                try
                {
                    string md = "Select * from Departamento where borrado like '0'";
                    DataSet d = item.Ejecutar(md);
                    DataTable table = null;

                    if (d.Tables.Count > 0)
                    {
                        table = d.Tables[0];
                    }

                    Console.WriteLine("Lista de Departamento disponibles");
                    foreach (DataRow drCurrent in table.Rows)
                    {
                        Console.WriteLine("{0}- Nombre: {1}", Convert.ToInt32(drCurrent["id"].ToString()),
                            drCurrent["nombre"].ToString());
                    }
                    id = Convert.ToInt16(Console.ReadLine());
                    lon = table.Rows.Count;
                }
                catch (Exception e)
                {
                    Console.WriteLine("\n\t\t\tEntra un numero...");
                }
            } while (id < 0);

            Console.Write("\n\t\tDigite el nombre del departamento: ");
            nombre = Console.ReadLine();
            Console.Write("\n\t\tDigite el estatus del departamento: ");
            estatus = Console.ReadLine();

            var departamento = new Departamento();
            departamento.Nombre = nombre;
            departamento.Estatus = estatus;
            departamento.Id = id;

            string cmd = string.Format("Exec spUpdateDepartamento '{0}','{1}','{2}','{3}','{4}'",
                departamento.Id, departamento.Nombre, departamento.Estatus, fechaModificacion, idUser);
            DataSet ds = item.Ejecutar(cmd);
            
            Console.Clear();
            Console.WriteLine("Se actualizó el Departamento");
            Console.WriteLine("Presione una tecla para continuar..");
            Console.ReadKey();
            Console.Clear();
        }

        public void RetrieveDepartment()
        {
            var item = new Conexion();
            string md = "Select * from Departamento where borrado like '0'";
            DataSet d = item.Ejecutar(md);
            DataTable table = null;

            if (d.Tables.Count > 0)
            {
                table = d.Tables[0];
            }

            Console.WriteLine("Lista de Departamento disponibles");
            foreach (DataRow drCurrent in table.Rows)
            {
                Console.WriteLine("{0}- Nombre: {1}", Convert.ToInt32(drCurrent["id"].ToString()),
                    drCurrent["nombre"].ToString());
            }

            Console.WriteLine("");
            Console.WriteLine("Presione una tecla para continuar..");
            Console.ReadKey();
            Console.Clear();
        }

        public void DeleteDepartment()
        {
            var item = new Conexion();
            int id = 0, lon = 1;
            do
            {
                Console.Write("\n\t\tElige del un departamento: ");
                try
                {
                    string md = "Select * from Departamento where borrado like '0'";
                    DataSet d = item.Ejecutar(md);
                    DataTable table = null;

                    if (d.Tables.Count > 0)
                    {
                        table = d.Tables[0];
                    }

                    Console.WriteLine("Lista de Departamento disponibles");
                    foreach (DataRow drCurrent in table.Rows)
                    {
                        Console.WriteLine("{0}- Nombre: {1}", Convert.ToInt32(drCurrent["id"].ToString()),
                            drCurrent["nombre"].ToString());
                    }
                    id = Convert.ToInt16(Console.ReadLine());
                    lon = table.Rows.Count;
                }
                catch (Exception e)
                {
                    Console.WriteLine("\n\t\t\tEntra un numero...");
                }
            } while (id < 0);

            string cmd = string.Format("Exec spDeleteDepartamento '{0}','{1}'",
                id, 1);
            DataSet ds = item.Ejecutar(cmd);
            
            Console.Clear();
            Console.WriteLine("Se eliminó el Departamento");
            Console.WriteLine("Presione una tecla para continuar..");
            Console.ReadKey();
            Console.Clear();
        }
    }
}
