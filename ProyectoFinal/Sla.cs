﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace ProyectoFinal
{
    class Sla
    {
        int _Id;
        string _Descripcion;
        int _CantidadHoras;
        string _Nombre;
        string _Estatus;
        string _FechaModificacion;
        string _CreadorPor;
        string _ModificadoPor;

        public Sla()
        {

        }

        public Sla(int id, string descripcion, int cantidadHoras, string nombre, string estatus, string fechaModificacion, string creadorPor, string modificadoPor)
        {
            Id = id;
            Descripcion = descripcion;
            CantidadHoras = cantidadHoras;
            Nombre = nombre;
            Estatus = estatus;
            FechaModificacion = fechaModificacion;
            CreadorPor = creadorPor;
            ModificadoPor = modificadoPor;
        }

        public int Id { get => _Id; set => _Id = value; }
        public string Descripcion { get => _Descripcion; set => _Descripcion = value; }
        public int CantidadHoras { get => _CantidadHoras; set => _CantidadHoras = value; }
        public string Nombre { get => _Nombre; set => _Nombre = value; }
        public string Estatus { get => _Estatus; set => _Estatus = value; }
        public string FechaModificacion { get => _FechaModificacion; set => _FechaModificacion = value; }
        public string CreadorPor { get => _CreadorPor; set => _CreadorPor = value; }
        public string ModificadoPor { get => _ModificadoPor; set => _ModificadoPor = value; }

        public void addSla(Login login)
        {
            var item = new Conexion();
            string descripcion = "", estatus = "";
            int cantHoras = 0;
            Console.Write("\n\t\tDigite la descripcion: ");
            descripcion = Console.ReadLine();
            Console.Write("\n\t\tDigite el estatus: ");
            estatus = Console.ReadLine();
            Console.Write("\n\t\tDigite la cantidad de horas: ");
            cantHoras = Convert.ToInt16(Console.ReadLine());

            using (SqlConnection conn = new SqlConnection(Conexion.con))
            {
                using (SqlCommand cmd = new SqlCommand("SpInsertSla", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("@descripcion", descripcion));
                    cmd.Parameters.Add(new SqlParameter("@cantidadHoras", cantHoras));
                    cmd.Parameters.Add(new SqlParameter("@estatus", estatus));
                    cmd.Parameters.Add(new SqlParameter("@fechaRegistro", DateTime.Now));
                    cmd.Parameters.Add(new SqlParameter("@creadoPor", login.Id));

                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }

           /* string cmd = string.Format("Exec spInsertSla '{0}','{1}','{2}','{3}'",
                descripcion, cantHoras, estatus, login.Id);
            DataSet ds = item.Ejecutar(cmd);*/

            Console.Clear();
            Console.WriteLine("Se agregó un Sla");
            Console.WriteLine("Presione una tecla para continuar..");
            Console.ReadKey();
            Console.Clear();
        }

        public void updateSla(Login login)
        {
            var item = new Conexion();
            string descripcion = "", estatus = "";
            int cantHoras = 0, id = 0, lon = 1;
            do
            {
                Console.Write("\n\t\tElige un Sla: ");
                try
                {
                    string md = "Select * from Sla where borrado like '0'";
                    string mdd = "Select * from Sla ";
                    DataSet d = item.Ejecutar(md);
                    DataSet dc = item.Ejecutar(mdd);
                    DataTable table = null;
                    DataTable table2 = null;

                    if (d.Tables.Count > 0)
                    {
                        table = d.Tables[0];
                        table2 = dc.Tables[0];
                        lon = table2.Rows.Count;
                    }

                    Console.WriteLine("Lista de Departamento disponibles");
                    Console.WriteLine("");
                    foreach (DataRow drCurrent in table.Rows)
                    {
                        Console.WriteLine("{0}- Descripcion: {1}", Convert.ToInt32(drCurrent["id"].ToString()),
                            drCurrent["descripcion"].ToString());
                    }
                    id = Convert.ToInt16(Console.ReadLine());
                }
                catch (Exception e)
                {
                    Console.WriteLine("\n\t\t\tEntra un numero...");
                }
            } while (id < 0 || id > lon);
            
            Console.Write("\n\t\tDigite la descripcion: ");
            descripcion = Console.ReadLine();
            Console.Write("\n\t\tDigite el estatus: ");
            estatus = Console.ReadLine();
            Console.Write("\n\t\tDigite la cantidad de horas: ");
            cantHoras = int.Parse(Console.ReadLine());
            using (SqlConnection conn = new SqlConnection(Conexion.con))
            {
                using (SqlCommand cmd = new SqlCommand("SpUpdateSla", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("@id", id));
                    cmd.Parameters.Add(new SqlParameter("@descripcion", descripcion));
                    cmd.Parameters.Add(new SqlParameter("@cantidadHoras", cantHoras));
                    cmd.Parameters.Add(new SqlParameter("@estatus", estatus));
                    cmd.Parameters.Add(new SqlParameter("@fechaModificacion", DateTime.Now));
                    cmd.Parameters.Add(new SqlParameter("@modificadoPor", login.Id));

                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
            /* string cmd = string.Format("Exec spUpdateSla '{0}','{1}','{2}','{3}'",
                 id, descripcion, cantHoras, estatus, DateTime.Now, login.Id);
             DataSet ds = item.Ejecutar(cmd);*/

            Console.Clear();
            Console.WriteLine("Se actualizó el Sla");
            Console.WriteLine("Presione una tecla para continuar..");
            Console.ReadKey();
            Console.Clear();
        }

        public void retrieve()
        {
            var item = new Conexion();
            string md = "Select * from Sla where borrado like '0'";
            DataSet d = item.Ejecutar(md);
            DataTable table = null;

            if (d.Tables.Count > 0)
            {
                table = d.Tables[0];
            }

            Console.WriteLine("Lista de SLA disponibles");
            Console.WriteLine("");
            foreach (DataRow drCurrent in table.Rows)
            {
                Console.WriteLine("{0}- Descripcion: {1}, Cantidad de Horas: {2}, Estado: {3}", Convert.ToInt32(drCurrent["id"].ToString()),
                    drCurrent["descripcion"].ToString(), drCurrent["cantidadHoras"].ToString(), drCurrent["estatus"].ToString());
            }

            Console.WriteLine("");
            Console.WriteLine("Presione una tecla para continuar..");
            Console.ReadKey();
            Console.Clear();
        }

        public void deleteSla()
        {
            var item = new Conexion();
            int id = 0, lon = 1;
            do
            {
                Console.Write("\n\t\tElige un Sla: ");
                try
                {
                    string md = "Select * from Sla where borrado like '0'";
                    string mdd = "Select * from Sla ";
                    DataSet d = item.Ejecutar(md);
                    DataSet dc = item.Ejecutar(mdd);
                    DataTable table = null;
                    DataTable table2 = null;

                    if (d.Tables.Count > 0)
                    {
                        table = d.Tables[0];
                        table2 = dc.Tables[0];
                        lon = table2.Rows.Count;
                    }

                    Console.WriteLine("Lista de Sla disponibles");
                    Console.WriteLine("");
                    foreach (DataRow drCurrent in table.Rows)
                    {   
                        Console.WriteLine("{0}- Descripcion: {1}", Convert.ToInt32(drCurrent["id"].ToString()),
                            drCurrent["descripcion"].ToString());
                    }
                    id = Convert.ToInt16(Console.ReadLine());
                   
                }
                catch (Exception e)
                {
                    Console.WriteLine("\n\t\t\tEntra un numero...");
                }
            } while (id < 0 || id > lon);
           
            string cmd = string.Format("Exec spDeleteSla '{0}','{1}'",
                id, 1);
            DataSet ds = item.Ejecutar(cmd);

            Console.Clear();
            Console.WriteLine("Se eliminó el Sla");
            Console.WriteLine("Presione una tecla para continuar..");
            Console.ReadKey();
            Console.Clear();
        }
    }
}
